# tree-sitter-nginx

[NGINX](https://nginx.org) & [OpenResty](https://openresty.org/) Configuration grammar for [tree-sitter](https://tree-sitter.github.io/tree-sitter/).